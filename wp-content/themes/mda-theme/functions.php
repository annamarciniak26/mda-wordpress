<?php

function mda_add_theme_styles()
{
    wp_enqueue_style(
        'bootstrap',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
        [],
        '4.3.1',
        'all'
    );
    wp_enqueue_style(
        'custom_style',
        get_template_directory_uri() . '/css/style.css',
        [],
        '1',
        'all'
    );
    wp_enqueue_style(
        'responsive_style',
        get_template_directory_uri() . '/css/responsive.css',
        [],
        '1',
        'all'
    );
}
add_action('wp_enqueue_scripts', 'mda_add_theme_styles');

add_theme_support('custom-logo');

function mda_custom_logo_setup()
{
    $defaults = [
        'flex-height' => true,
        'flex-width' => true,
        'unlink-homepage-logo' => true,
    ];

    add_theme_support('custom-logo', $defaults);
}

function mda_change_logo_class($html)
{
    $html = str_replace('custom-logo', 'header-logo-img', $html);

    return $html;
}

add_filter('get_custom_logo', 'mda_change_logo_class');

if (function_exists('acf_add_options_page')) {
    acf_add_options_page([
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => true,
    ]);

    acf_add_options_sub_page([
        'page_title' => 'Theme Banner Settings',
        'menu_title' => 'Banner',
        'parent_slug' => 'theme-general-settings',
    ]);

    acf_add_options_sub_page([
        'page_title' => 'Theme Header Bar Setting',
        'menu_title' => 'Header Bar',
        'parent_slug' => 'theme-general-settings',
    ]);

    acf_add_options_sub_page([
        'page_title' => 'Theme Footer Setting',
        'menu_title' => 'Footer',
        'parent_slug' => 'theme-general-settings',
    ]);
}

add_theme_support('post-thumbnails');