<?php get_header(); ?>
<?php if (have_rows('full_offers')): ?>
<?php while (have_rows('full_offers')):
    the_row(); ?>
<?php $title = get_sub_field('title'); ?>
<div id="full-offer" class="full-offer">
  <div class="container">
    <div class="row">
      <?php if ($title): ?>
      <h2 class="full-offer-title">
        <?php echo $title; ?>
      </h2>
      <?php endif; ?>
    </div>
    <?php if (have_rows('offer')): ?>
    <div class="full-offer-container row">
      <?php while (have_rows('offer')):
          the_row(); ?>
      <?php
      $offer_image = get_sub_field('offer_image');
      $offer_name = get_sub_field('offer_name');
      $offer_description = get_sub_field('offer_description');
      ?>
      <div class="full-offer-tile col-lg-4">
        <div class="full-offer-box row">
          <div class="full-offer-img-box col-2">
            <?php if ($offer_image): ?>
            <img 
                 src="<?php echo $offer_image['sizes']['medium']; ?>" 
                 alt="<?php echo $offer_image['alt']; ?>" 
                 />
            <?php endif; ?>
          </div>
          <div class="full-offer-content-box col-10">
            <?php if ($offer_name): ?>
            <h3 class="full-offer-content-title">
              <?php echo $offer_name; ?>
            </h3>
            <?php endif; ?>
            <?php if ($offer_description): ?>
            <p class="full-offer-content">
              <?php echo $offer_description; ?>
            </p>
            <?php endif; ?>
            <?php if (have_rows('links')): ?>
            <div class="full-offer-link-box">
              <?php while (have_rows('links')):
                  the_row(); ?> 
              <?php
              $link_text = get_sub_field('link_text');
              $link = get_sub_field('link');
              ?>
              <?php if ($link): ?>
              <a href="<?php echo $link; ?>" class="full-offer-link">
                <?php echo $link_text; ?>
              </a>
              <?php endif; ?>
              <?php
              endwhile; ?>
            </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <?php
      endwhile; ?>
    </div>
    <?php endif; ?>
    <div class="full-offer-indicators">
      <span class="full-offer-indicators-bullet">
      </span>
      <span class="full-offer-indicators-bullet">
      </span>
      <span class="full-offer-indicators-bullet">
      </span>
      <span class="full-offer-indicators-bullet">
      </span>
    </div>
  </div>
  <?php $tool = get_sub_field('tool_image'); ?>
  <?php if ($tool): ?>
  <div class="full-offer-tool">
    <img 
    class="full-offer-tool-img" 
    src="<?php echo $tool['sizes']['large']; ?>" 
    alt="<?php echo $tool['alt']; ?>" />
  </div>
  <?php endif; ?>
</div>
<?php
endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
