<?php get_header(); ?>

<div class="sub-page">
    <div class="sub-page-container container">
        <h1 class="sub-page-title"><?php the_title(); ?><h1>
        <?php
        while (have_posts()):
            the_post(); ?>
        <div class="sub-page-content">
            <?php the_content(); ?>
        </div>
        <?php
        endwhile;
        wp_reset_query();
        ?>
    </div>
</div>

<?php get_footer(); ?>
