<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Stomatologia - Persona</title>

    <?php wp_head(); ?>
  </head>

  <body>
    <div id="header" class="header" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
      <div class="header-bar d-flex justify-content-between align-items-center">
        <div class="header-logo">
        <?php if (function_exists('the_custom_logo')) {
            the_custom_logo();
        } ?>
        </div>
        <?php $toggle = get_field('toggle', 'option'); ?>
        <?php if ($toggle): ?>
        <a href="#" class="header-toggle">
            <img 
            class="header-toggle-icon" 
            src="<?php echo $toggle['sizes']['medium']; ?>" 
            alt="<?php echo $toggle['alt']; ?>" />
        </a>
        <?php endif; ?>
        <div id="header-info" class="header-info row align-items-center">
          <div class="header-info-item header-info-item-language">
            <p><?php the_field('header_language', 'option'); ?></p>
          </div>
          <div class="header-info-item header-info-item-open row">
              <?php $open_hours = get_field('open_hours', 'option'); ?>
            <p><?php echo $open_hours['text']; ?>
              <img
                class="header-info-item-icon"
                src="<?php echo $open_hours['clock']['sizes']['medium']; ?>"
                alt="<?php echo $open_hours['clock']['alt']; ?>"
              />
              <span class="header-info-item-time">
                <span class="header-info-item-time-big"><?php echo $open_hours[
                    'hour'
                ]; ?></span>
                <span class="header-info-item-time-small">00</span>
              </span>
            </p>
          </div>
          <div class="header-info-item header-info-item-button">
          <?php $reservation = get_field('reservation', 'option'); ?>
          <?php if ($reservation): ?>
            <a href="<?php echo $reservation[
                'reservation_link'
            ]; ?>" class="btn btn-pink">
                <?php echo $reservation['reservation_text']; ?>
            </a>
            <?php endif; ?>
          </div>
        </div>
      </div>

      <div class="header-banner">
        <div class="container">
          <h1 class="header-banner-title"><?php the_field(
              'banner_title',
              'option'
          ); ?></h1>
          <div class="row">
            <?php
            $arrow = get_field('arrows', 'option');
            $arrow_left = $arrow['arrow_left'];
            $arrow_right = $arrow['arrow_right'];
            $arrow_down = $arrow['arrow_down'];
            ?>
            <?php if ($arrow_left): ?>
            <img
              class="header-banner-arrow-icon"
              src="<?php echo $arrow_left['sizes']['medium']; ?>"
              alt="<?php echo $arrow_left['alt']; ?>"
            />
            <?php endif; ?>
            <?php if ($arrow_right): ?>
            <img
              class="header-banner-arrow-icon"
              src="<?php echo $arrow_right['sizes']['medium']; ?>"
              alt="<?php echo $arrow_right['alt']; ?>"
            />
            <?php endif; ?>
            <h3 class="header-banner-subtitle">
                <?php the_field('banner_subtitle', 'option'); ?>
            </h3>
            <?php $mobile_button = get_field('mobile_button', 'option'); ?>
          </div>
          <?php if ($mobile_button): ?>
            <div class="header-banner-button-box">
            <a href="<?php echo $mobile_button[
                'mobile_button_link'
            ]; ?>" class="header-banner-button btn btn-dark"
              ><?php echo $mobile_button['mobile_button_text']; ?></a
            >
          </div>
            <?php endif; ?>

        
          <div class="header-banner-offer row">

          <?php if (have_rows('banner_offers', 'option')): ?>
            <?php while (have_rows('banner_offers', 'option')):
                the_row(); ?>
            
            <?php if (have_rows('offer')): ?>
            <?php while (have_rows('offer')):
                the_row(); ?>
            <?php
            $offer_title = get_sub_field('offer_title');
            $offer_description = get_sub_field('offer_description');
            $offer_image = get_sub_field('offer_image');
            $link = get_sub_field('link');
            $link_text = get_sub_field('link_text');
            ?>
            <div class="header-banner-tile col-lg-4">
              <div class="banner-offer row">
                <div class="banner-offer-img-box col-lg-3">
                    <?php if ($offer_image): ?>
                  <img 
                  src="<?php echo $offer_image['sizes']['medium']; ?>" 
                  alt="<?php echo $offer_image['alt']; ?>" 
                  />
                  <?php endif; ?>
                </div>
                <div class="banner-offer-content-box col-lg-8">
                    <?php if ($offer_title): ?>
                  <h4 class="banner-offer-content-title"><?php echo $offer_title; ?></h4>
                  <?php endif; ?>
                  <?php if ($offer_description): ?>
                  <p><?php echo $offer_description; ?></p>
                  <?php endif; ?>
                  <?php if ($link): ?>
                    <a href="<?php echo $link; ?>" class="btn btn-dark"><?php echo $link_text; ?></a>
                <?php endif; ?>
                </div>
              </div>
            </div>
            <?php
            endwhile; ?>
            <?php endif; ?>
            <?php
            endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
        
        <div class="header-banner-bottom">
          <div class="row justify-content-between align-items-center">
            <div class="header-banner-bottom-arrow-box">
                <?php if ($arrow_down): ?>
                <img
                class="header-banner-bottom-arrow"
                src="<?php echo $arrow_down['sizes']['medium']; ?>"
                alt="<?php echo $arrow_down['alt']; ?>"
                />
                <?php endif; ?>
            </div>
            <?php $phone = get_field('phone', 'option'); ?>
            <?php if ($phone); ?>
            <a 
            href="<?php echo $phone['phone_link']; ?>" 
            class="btn-circle btn-pink">
              <img 
              class="btn-circle-icon" 
              src="<?php echo $phone['phone_image']['sizes']['medium']; ?>" 
              alt="<?php echo $phone['phone_image']['alt']; ?>" />
            </a>
            <? endif; ?>
          </div>
        </div>
      </div>

      <?php $tooth = get_field('tooth', 'option'); ?>
      <?php if ($tooth): ?>
      <div class="full-offer-tooth">
        <img 
        class="full-offer-tooth-img" 
        src="<?php echo $tooth['sizes']['large']; ?>" 
        alt="<?php echo $tooth['alt']; ?>" />
      </div>
      <?php endif; ?>
    </div>