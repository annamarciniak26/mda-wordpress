<?php get_header(); ?>
<div class="page-404">
  <div class="page-404-container container">
    <div class="page-404-title-box row">
      <h2 class="page-404-title">404
      </h2>
    </div>
    <div class="page-404-content">
      <p>Ups... coś poszło nie tak
      </p>
      <a href="<?php echo get_home_url(); ?>" class="btn btn-pink">
        Powrót
      </a>
    </div>
  </div>
</div>
<?php get_footer(); ?>
