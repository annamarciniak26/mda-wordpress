<div class="footer">
  <div class="footer-box">
    <p class="footer-copy">Copyright 
      <?php echo date('Y'); ?> by 
      <?php the_field('copyright_by', 'option'); ?>
    </p>        
    <?php $credit = get_field('credit', 'option'); ?>
    <?php if ($credit): ?>
    <p class="footer-credit">
      <?php echo $credit['credit_text']; ?>
      <span class="footer-credit-logo">
        <img 
             class="footer-credit-logo" 
             src="<?php echo $credit['credit_image']['sizes']['thumbnail']; ?>" 
             alt="<?php echo $credit['credit_image']['alt']; ?>"
             />
      </span>
    </p>
    <?php endif; ?>
  </div>
</div>
</body>
</html>